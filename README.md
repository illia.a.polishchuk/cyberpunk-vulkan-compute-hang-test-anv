# cyberpunk-vulkan-compute-hang-test-anv
## Building
```bash
mkdir build
cd build
cmake ..
cmake --build .
```
## Running with ANV hangs with 90% chance 
```bash
export VK_ICD_FILENAMES=/home/illia/sources/mesa/build/share/vulkan/icd.d/intel_icd.x86_64.json

./cyberpunk-vulkan-compute-hang-test-anv
# HANG!!!
```

## Running with LVP does not hang
```bash
export VK_ICD_FILENAMES=/home/illia/sources/mesa/build/share/vulkan/icd.d/lvp_icd.x86_64.json

./cyberpunk-vulkan-compute-hang-test-anv
# SUCCESS
```
